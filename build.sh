mkdir build

gcc -Wall -Wextra src/tp1.c -o build/tp1
gcc -Wall -Wextra src/tp2.c src/heap.c -o build/tp2 -lm
gcc -Wall -Wextra src/tp3.c -o build/tp3
gcc -Wall -Wextra src/tp4.c src/graph.c src/list.c src/queue.c -o build/tp4
gcc -Wall -Wextra src/tp5.c src/graph.c src/list.c -o build/tp5
gcc -Wall -Wextra src/tp6.c src/graph.c src/list.c src/heap.c -o build/tp6 -lm