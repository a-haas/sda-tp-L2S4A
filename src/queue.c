#include <stdlib.h>
#include <stdbool.h>
#include "queue.h"

Queue new_queue(unsigned max_size) {
    Queue q = malloc(sizeof(struct queue));
    q->max_size = max_size;
    q->queue = malloc(max_size * sizeof(unsigned));
    q->front = -1;
    q->rear = -1;

    return q;
}

void free_queue(Queue q) {
    free(q->queue);
    free(q);
}

//Function created to handle enqueue
void enqueue(Queue q, unsigned item){
    //The first element condition
    if(q->front == -1){
        q->front = 0;
    }
    
    q->rear += 1;
    q->queue[q->rear] = item;
}

//Function created to handle dequeue
unsigned dequeue(Queue q){
    unsigned res = q->queue[q->front];
    q->front += 1;

    //Only happens when the last element was dequeued
    if(q->front > q->rear){
        q->front = -1;
        q->rear = -1;
    }

    return res;
}

bool is_queue_empty(Queue q) {
    return (q->front < 0 && q->rear < 0);
}