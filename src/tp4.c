#include <stdio.h>
#include <stdlib.h>
#include <limits.h>
#include <assert.h>
#include <time.h>
#include <stdbool.h>

#include "queue.h"
#include "graph.h"

void matrix_dfs_rec(Matrix m, unsigned src, bool *visited) {
    // current node treatment
    // ...
    // mark current node
    visited[src] = true;
    List next = matrix_vertex_successors(m, src);
    while(next != NULL) {
        unsigned dest = ((Edge) next->value)->id;
        if(!visited[dest]) {
            matrix_dfs_rec(m, dest, visited);
        }
        next = next->next;
    }
}

void matrix_dfs(Matrix m) {
    bool *visited = malloc(sizeof(bool) * m->nb_vertices);
    
    for(unsigned i = 0; i < m->nb_vertices; i++) {
        visited[i] = false;
    }
    
    for(unsigned i = 0; i < m->nb_vertices; i++) {
        if(!visited[i]) {
            matrix_dfs_rec(m, i, visited);
        }
    }

    free(visited);
}

void matrix_bfs(Matrix m) {
    Queue q = new_queue(m->nb_vertices);
    bool *visited = malloc(sizeof(bool) * m->nb_vertices);
    for(unsigned i = 0; i < m->nb_vertices; i++) {
        visited[i] = false;
    }

    for(unsigned i = 0; i < m->nb_vertices; i++) {
        if(!visited[i]) {
            enqueue(q, i);
            visited[i] = true;
        }

        while(!is_queue_empty(q)) {
            unsigned src = dequeue(q);
            visited[src] = true;

            List next = matrix_vertex_successors(m, src);
            while(next != NULL) {
                unsigned dest = ((Edge) next->value)->id;
                if(!visited[dest]) {
                    enqueue(q, dest);
                    visited[dest] = true;
                }
                next = next->next;
            }
        }
    }

    free_queue(q);
}

void adj_dfs_rec(AdjacencyList a, unsigned src, bool *visited) {
    // current node treatment
    // ...
    // mark current node
    visited[src] = true;
    List next = adjacency_list_successors(a, src);
    while(next != NULL) {
        unsigned dest = ((Edge) next->value)->id;
        if(!visited[dest]) {
            adj_dfs_rec(a, dest, visited);
        }
        next = next->next;
    }
}


void adj_dfs(AdjacencyList a) {
    bool *visited = malloc(sizeof(bool) * a->nb_vertices);
    
    for(unsigned i = 0; i < a->nb_vertices; i++) {
        visited[i] = false;
    }
    
    for(unsigned i = 0; i < a->nb_vertices; i++) {
        if(!visited[i]) {
            adj_dfs_rec(a, i, visited);
        }
    }

    free(visited);
}

void adj_bfs(AdjacencyList a) {
    Queue q = new_queue(a->nb_vertices);
    bool *visited = malloc(sizeof(bool) * a->nb_vertices);
    for(unsigned i = 0; i < a->nb_vertices; i++) {
        visited[i] = false;
    }

    for(unsigned i = 0; i < a->nb_vertices; i++) {
        if(!visited[i]) {
            enqueue(q, i);
            visited[i] = true;
        }

        while(!is_queue_empty(q)) {
            unsigned src = dequeue(q);
            visited[src] = true;

            List next = adjacency_list_successors(a, src);
            while(next != NULL) {
                unsigned dest = ((Edge) next->value)->id;
                if(!visited[dest]) {
                    enqueue(q, dest);
                    visited[dest] = true;
                }
                next = next->next;
            }
        }
    }
    free_queue(q);
}

void measure_is_in_matrix(char *filename, unsigned n, Matrix m) {
    FILE *file = fopen(filename, "a");
    double total_time = 0.0;
    
    for (int i = 0; i < 10; i++) {
		printf("i = %d\n", i);

        clock_t start_t, end_t;
        double total_t = 0.0;
        
        for(unsigned j = 0; j < 100; j++) {
            unsigned src = rand() % n;
            unsigned dest = rand() % n;

            start_t = clock();
            is_in_matrix(m, src, dest);
            end_t = clock();
            total_t += ((double)(end_t - start_t)) / (double)CLOCKS_PER_SEC;
        }
        total_time += total_t;
    }

    if (file != NULL) {
        fprintf(file, "%d %f\n", n, total_time / 10.0);
    }
    fclose(file);
}

void measure_next_matrix(char *filename, unsigned n, Matrix m) {
    FILE *file = fopen(filename, "a");
    double total_time = 0.0;
    
    for (int i = 0; i < 10; i++) {
		printf("i = %d\n", i);

        clock_t start_t, end_t;
        double total_t = 0.0;
        
        for(unsigned j = 0; j < 100; j++) {
            unsigned src = rand() % n;

            start_t = clock();
            matrix_vertex_successors(m, src);
            end_t = clock();
            total_t += ((double)(end_t - start_t)) / (double)CLOCKS_PER_SEC;
        }
        total_time += total_t;
    }

    if (file != NULL) {
        fprintf(file, "%d %f\n", n, total_time / 10.0);
    }
    fclose(file);
}

void measure_is_in_adjacency_list(char *filename, unsigned n, AdjacencyList a) {
    FILE *file = fopen(filename, "a");
    double total_time = 0.0;
    
    for (int i = 0; i < 10; i++) {
		printf("i = %d\n", i);
        
        clock_t start_t, end_t;
        double total_t = 0.0;
        
        for(unsigned j = 0; j < 100; j++) {
            unsigned src = rand() % n;
            unsigned dest = rand() % n;
            
            start_t = clock();
            is_in_adjacency_list(a, src, dest);
            end_t = clock();
            total_t += ((double)(end_t - start_t)) / (double)CLOCKS_PER_SEC;
        }
        total_time += total_t;
    }

    if (file != NULL) {
        fprintf(file, "%d %f\n", n, total_time / 10.0);
    }
    fclose(file);
}

void measure_next_adjacency_list(char *filename, unsigned n, AdjacencyList a) {
    FILE *file = fopen(filename, "a");
    double total_time = 0.0;
    
    for (int i = 0; i < 10; i++) {
		printf("i = %d\n", i);
        
        clock_t start_t, end_t;
        double total_t = 0.0;
        
        for(unsigned j = 0; j < 100; j++) {
            unsigned src = rand() % n;
            
            start_t = clock();
            adjacency_list_successors(a, src);
            end_t = clock();
            total_t += ((double)(end_t - start_t)) / (double)CLOCKS_PER_SEC;
        }
        total_time += total_t;
    }

    if (file != NULL) {
        fprintf(file, "%d %f\n", n, total_time / 10.0);
    }
    fclose(file);
}

void measure_dsf_adjacency_list(char *filename, unsigned n, AdjacencyList a) {
    FILE *file = fopen(filename, "a");
    double total_time = 0.0;
    
    for (int i = 0; i < 10; i++) {
		printf("i = %d\n", i);
        
        clock_t start_t, end_t;
        start_t = clock();
        adj_dfs(a);
        end_t = clock();
        total_time += ((double)(end_t - start_t)) / (double)CLOCKS_PER_SEC;;
    }

    if (file != NULL) {
        fprintf(file, "%d %f\n", n, total_time / 10.0);
    }
    fclose(file);
}

void measure_dsf_matrix(char *filename, unsigned n, Matrix m) {
    FILE *file = fopen(filename, "a");
    double total_time = 0.0;
    
    for (int i = 0; i < 10; i++) {
		printf("i = %d\n", i);
        
        clock_t start_t, end_t;
        start_t = clock();
        matrix_dfs(m);
        end_t = clock();
        total_time += ((double)(end_t - start_t)) / (double)CLOCKS_PER_SEC;;
    }

    if (file != NULL) {
        fprintf(file, "%d %f\n", n, total_time / 10.0);
    }
    fclose(file);
}

void measure_bfs_adjacency_list(char *filename, unsigned n, AdjacencyList a) {
    FILE *file = fopen(filename, "a");
    double total_time = 0.0;
    
    for (int i = 0; i < 10; i++) {
		printf("i = %d\n", i);
        
        clock_t start_t, end_t;
        start_t = clock();
        adj_bfs(a);
        end_t = clock();
        total_time += ((double)(end_t - start_t)) / (double)CLOCKS_PER_SEC;;
    }

    if (file != NULL) {
        fprintf(file, "%d %f\n", n, total_time / 10.0);
    }
    fclose(file);
}

void measure_bfs_matrix(char *filename, unsigned n, Matrix m) {
    FILE *file = fopen(filename, "a");
    double total_time = 0.0;
    
    for (int i = 0; i < 10; i++) {
		printf("i = %d\n", i);
        
        clock_t start_t, end_t;
        start_t = clock();
        matrix_bfs(m);
        end_t = clock();
        total_time += ((double)(end_t - start_t)) / (double)CLOCKS_PER_SEC;;
    }

    if (file != NULL) {
        fprintf(file, "%d %f\n", n, total_time / 10.0);
    }
    fclose(file);
}

/** MAIN */

int main() {
    
    unsigned n = 5;
    for (int k = 0; k < 11; k++) {
        n *= 2;
        
		printf("n = %d\n", n);
        
        AdjacencyList adj_random = new_adjacency_list(n);
        Matrix matrix_random = new_matrix(n);
        AdjacencyList adj_full = new_adjacency_list(n);
        Matrix matrix_full = new_matrix(n);

        // Initializes random number generator
        time_t t;
        srand((unsigned)time(&t));
        
        // add 10 edges for every vertex 
        for (unsigned i = 0; i < n; i++){
            int val = rand() % n;

            for(unsigned j = 0; j < 10; j++) {
                adjacency_add_edge(adj_random, i, new_edge(val, 1));
                matrix_add_edge(matrix_random, i, val);
            }
        }

        // create full graph
        for(unsigned i = 0; i < n; i++) {
            for(unsigned j = 0; j < n; j++) {
                adjacency_add_edge(adj_full, i, new_edge(j, 1));
                matrix_add_edge(matrix_full, i, j);
            }
        }

        measure_is_in_adjacency_list("AdjacencyList_low_density.time", n, adj_random);
        measure_is_in_matrix("Matrix_low_density.time", n, matrix_random);
        measure_is_in_adjacency_list("AdjacencyList_high_density.time", n, adj_full);
        measure_is_in_matrix("Matrix_high_density.time", n, matrix_full);

        measure_next_adjacency_list("AdjacencyList_next_low_density.time", n, adj_random);
        measure_next_matrix("Matrix_next_low_density.time", n, matrix_random);
        measure_next_adjacency_list("AdjacencyList_next_high_density.time", n, adj_full);
        measure_next_matrix("Matrix_next_high_density.time", n, matrix_full);

        measure_dsf_adjacency_list("AdjacencyList_dsf_low_density.time", n, adj_random);
        measure_dsf_matrix("Matrix_dsf_low_density.time", n, matrix_random);
        measure_dsf_adjacency_list("AdjacencyList_dsf_high_density.time", n, adj_full);
        measure_dsf_matrix("Matrix_dsf_high_density.time", n, matrix_full);

        measure_bfs_adjacency_list("AdjacencyList_bfs_low_density.time", n, adj_random);
        measure_bfs_matrix("Matrix_bfs_low_density.time", n, matrix_random);
        measure_bfs_adjacency_list("AdjacencyList_bfs_high_density.time", n, adj_full);
        measure_bfs_matrix("Matrix_bfs_high_density.time", n, matrix_full);

        free_adjacency(adj_random);
        free_adjacency(adj_full);
        free_matrix(matrix_full);
        free_matrix(matrix_random);
    }

    return 0;
}
