#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <limits.h>
#include <stdbool.h>
#include <assert.h>

void bubble_sort(int *T, unsigned n) {
    for (unsigned i = n - 1; i > 0; i--) {
        for (unsigned j = 0; j < i; j++) {
            if (T[j] > T[j + 1]){
                int temp = T[j];
                T[j] = T[j + 1];
                T[j + 1] = temp;
            }
        }
    }
}

void selection_sort(int *T, unsigned n) {
    for (unsigned i = 0; i < n; i++) {
        int max = i;
        for (unsigned j = i; j < n; j++) {
            if (T[j] < T[max]) {
                max = j;
            }
        }
        int tmp = T[max];
        T[max] = T[i];
        T[i] = tmp;
    }
}

void insertion_sort(int *T, unsigned n) {
    for (unsigned i = 1; i < n; i++) {
        int AInserer = T[i];
        unsigned j = i;
        while ((j > 0) && (T[j - 1] > AInserer)) {
            // Décalage vers la droite
            T[j] = T[j - 1];
            j--;
        }
        T[j] = AInserer;
    }
}

bool array_is_sorted(int *T, unsigned n) {
    for(unsigned i = 0; i < (n-1); i++) {
        if(T[i] > T[i + 1]) {
            return false;
        }
    }

    return true;
}

int main() {
    
    FILE *bubble_file = NULL;
    bubble_file = fopen("BubbleSort.time", "a");
    
    FILE *selection_file = NULL;
    selection_file = fopen("SelectSort.time", "a");
    
    FILE *insertion_file = NULL;
    insertion_file = fopen("InsertSort.time", "a");

    unsigned n = 5;
    for (int k = 0; k < 14; k++)
    {
        n *= 2;
        double current_total_time_bubble = 0.0;
        double current_total_time_selection = 0.0;
        double current_total_time_insertion = 0.0;
        printf("n = %d\n", n);
        for (int i = 0; i < 10; i++)
        {
            printf("i = %d\n", i);
            int T_bubble[n];
            int T_insertion[n];
            int T_select[n];

            time_t t;
            
            srand((unsigned)time(&t)); // Initializes random number generator
            for (unsigned i = 0; i < n; i++){
                int val = rand() % INT_MAX;
                T_bubble[i] = val;
                T_insertion[i] = val;
                T_select[i] = val;
            }

            clock_t start_t, end_t;
            double total_t;
            
            assert(!array_is_sorted(T_select, n));
            start_t = clock();
            selection_sort(T_select, n);
            end_t = clock();
            assert(array_is_sorted(T_select, n));

            total_t = ((double)(end_t - start_t)) / (double)CLOCKS_PER_SEC;
            current_total_time_selection += total_t;
            
            assert(!array_is_sorted(T_insertion, n));
            start_t = clock();
            insertion_sort(T_insertion, n);
            end_t = clock();
            assert(array_is_sorted(T_insertion, n));
            total_t = ((double)(end_t - start_t)) / (double)CLOCKS_PER_SEC;
            current_total_time_insertion += total_t;
            
            assert(!array_is_sorted(T_bubble, n));
            start_t = clock();
            bubble_sort(T_bubble, n);
            end_t = clock();
            assert(array_is_sorted(T_bubble, n));
            total_t = ((double)(end_t - start_t)) / (double)CLOCKS_PER_SEC;
            current_total_time_bubble += total_t;
        }
        current_total_time_bubble /= 10.0;
        if (bubble_file != NULL)
            fprintf(bubble_file, "%d %f\n", n, current_total_time_bubble);
        
        current_total_time_selection /= 10.0;
        if (selection_file != NULL)
            fprintf(selection_file, "%d %f\n", n, current_total_time_selection);
        
        current_total_time_insertion /= 10.0;
        if (insertion_file != NULL)
            fprintf(insertion_file, "%d %f\n", n, current_total_time_insertion);
    }

    fclose(bubble_file);
    fclose(selection_file);
    fclose(insertion_file);
    return 0;
}
