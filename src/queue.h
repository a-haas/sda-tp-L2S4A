#ifndef __QUEUE_H
#define __QUEUE_H

typedef struct queue {
    unsigned *queue;
    unsigned max_size;
    int front;
    int rear;
} *Queue;

Queue new_queue(unsigned max_size);
void free_queue(Queue q);
void enqueue(Queue q, unsigned item);
unsigned dequeue(Queue q);
bool is_queue_empty(Queue q);

#endif