#ifndef __LIST_H
#define __LIST_H

typedef struct list {
    unsigned key;
    void* value;
    struct list* next;
} *List;

List new_list(unsigned key, void* value);
void free_list(List l);
List add_to_list(List list, unsigned key, void* value);
List remove_from_list(List list, unsigned key);

#endif