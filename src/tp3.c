#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <limits.h>
#include <assert.h>
#include <stdbool.h>

/** NODE STRUCTURE */

typedef struct node
{
	unsigned value;
	struct node *left;
	struct node *right;
	unsigned height;
} * Node;

Node new_node(unsigned value) {
    Node new = malloc(sizeof(struct node));
    new->value = value;
    new->height = 1;
    new->left = NULL;
    new->right = NULL;

    return new;
}

void free_node(Node node)
{
	if (node != NULL) {
		free_node(node->left);
		free_node(node->right);
		free(node);
	}
}

unsigned node_height(Node node) {
    return node == NULL ? 0 : node->height;
}

bool is_bst_valid(Node node) {
    if(node == NULL) {
        return true;
    }

    if(node->left != NULL && node->left->value >= node->value) {
        return false;
    }

    if(node->right != NULL && node->right->value < node->value) {
        return false;
    }

    return is_bst_valid(node->left) && is_bst_valid(node->right);
}

/** BST */

Node insert_bst(Node node, unsigned value) {
    if(node == NULL) {
        return new_node(value);
    }

    if(value < node->value) {
        node->left = insert_bst(node->left, value);
    }
    else {
        node->right = insert_bst(node->right, value);
    }

    return node;
}

Node bst(unsigned* array, unsigned n) {
    Node root = NULL;
    for(unsigned i = 0; i < n; i++) {
        root = insert_bst(root, array[i]);
    }

    return root;
}

/** AVL */

void update_height(Node node) {
    if(node == NULL) {
        return;
    }

    node->height = 1 + (node_height(node->left) > node_height(node->right) ? node_height(node->left) : node_height(node->right));
}

unsigned balance(Node node) {
    return node == NULL ? 0 : node_height(node->left) - node_height(node->right);
}

Node right_rotate(Node node) {
    Node pivot = node->left;
    Node tmp = pivot->right;
    
    pivot->right = node;
    node->left = tmp;

    // update their heights
    update_height(node);
    update_height(pivot);

    return pivot;
}

Node left_rotate(Node node) {
    Node pivot = node->right;
    Node tmp = pivot->left;
    
    pivot->left = node;
    node->right = tmp;

    // update their heights
    update_height(node);
    update_height(pivot);

    return pivot;
} 

Node insert_avl(Node node, unsigned value) {
    if(node == NULL) {
        return new_node(value);
    }

    if(value < node->left->value) {
        node->left = insert_avl(node->left, value);
    }
    else {
        node->right = insert_avl(node->right, value);
    }
    
    update_height(node);
    int balance_factor = balance(node);
    assert(balance_factor <= 2 && balance_factor >= -2);

    if(balance_factor < -1 && value > node->right->value) {
        return left_rotate(node);
    }
    
    if(balance_factor > 1 && value < node->left->value) {
        return right_rotate(node);
    }
    
    if(balance_factor < -1 && value < node->right->value) {
        node->right = right_rotate(node->right);
        return left_rotate(node);
    }
    
    if(balance_factor > 1 && value > node->left->value) {
        node->left = left_rotate(node->left);
        return right_rotate(node);
    }

    return node;
}

Node avl(unsigned* array, unsigned n) {
    Node root = NULL;
    for(unsigned i = 0; i < n; i++) {
        insert_avl(root, array[i]);
    }

    return root;
}

/** MAIN */

int main() {
	
    FILE *bst_mean_file = NULL;
    bst_mean_file = fopen("BST_MeanTime.time", "a");
    
    FILE *bst_worst_file = NULL;
    bst_worst_file = fopen("BST_WorstTime.time", "a");
    
    FILE *avl_mean_file = NULL;
    avl_mean_file = fopen("AVL_MeanTime.time", "a");

	FILE *avl_worst_file = NULL;
    avl_worst_file = fopen("AVL_WorstTime.time", "a");

    unsigned n = 5;
    for (int k = 0; k < 14; k++) {
        n *= 2;
        double current_total_time_bst_mean = 0.0;
        double current_total_time_bst_worst = 0.0;
        double current_total_time_avl_mean = 0.0;
        double current_total_time_avl_worst = 0.0;
        
		printf("n = %d\n", n);
        
		for (int i = 0; i < 10; i++)
        {
            printf("i = %d\n", i);
            unsigned T_bst_worst[n];
            unsigned T_bst_mean[n];
            unsigned T_avl_mean[n];
            unsigned T_avl_worst[n];

            time_t t;

            // Initializes random number generator
            srand((unsigned)time(&t));
            for (unsigned j = 0; j < n; j++){
                int val = rand() % INT_MAX;
                T_bst_mean[j] = val;
                T_avl_mean[j] = val;
            }

			// generate worst case
            for(unsigned j = 0; j < n; j++) {
                T_bst_worst[j] = n + 1;
                T_avl_worst[j] = n + 1;
            }

            clock_t start_t, end_t;
            double total_t;
            
            start_t = clock();
            Node bst_mean = bst(T_bst_mean, n);
            end_t = clock();
            total_t = ((double)(end_t - start_t)) / (double)CLOCKS_PER_SEC;
            current_total_time_bst_mean += total_t;
            assert(is_bst_valid(bst_mean));
            
            start_t = clock();
            Node bst_worst = bst(T_bst_worst, n);
            end_t = clock();
            total_t = ((double)(end_t - start_t)) / (double)CLOCKS_PER_SEC;
            current_total_time_bst_worst += total_t;
            assert(is_bst_valid(bst_worst));
            
            start_t = clock();
            Node avl_mean = avl(T_avl_mean, n);
            end_t = clock();
            total_t = ((double)(end_t - start_t)) / (double)CLOCKS_PER_SEC;
            current_total_time_avl_mean += total_t;
            assert(is_bst_valid(avl_mean));

            start_t = clock();
            Node avl_worst = avl(T_avl_worst, n);
            end_t = clock();
            total_t = ((double)(end_t - start_t)) / (double)CLOCKS_PER_SEC;
            current_total_time_avl_worst += total_t;
            assert(is_bst_valid(avl_worst));

            free_node(bst_mean);
            free_node(bst_worst);
            free_node(avl_mean);
            free_node(avl_worst);
        }
        
        current_total_time_bst_mean /= 10.0;
        if (bst_mean_file != NULL)
            fprintf(bst_mean_file, "%d %f\n", n, current_total_time_bst_mean);
        
        current_total_time_bst_worst /= 10.0;
        if (bst_worst_file != NULL)
            fprintf(bst_worst_file, "%d %f\n", n, current_total_time_bst_worst);
        
        current_total_time_avl_mean /= 10.0;
        if (avl_mean_file != NULL)
            fprintf(avl_mean_file, "%d %f\n", n, current_total_time_avl_mean);

        current_total_time_avl_worst /= 10.0;
        if (avl_worst_file != NULL)
            fprintf(avl_worst_file, "%d %f\n", n, current_total_time_avl_worst);
    }

    fclose(bst_mean_file);
    fclose(bst_worst_file);
    fclose(avl_mean_file);
    fclose(avl_worst_file);

    return 0;
}
