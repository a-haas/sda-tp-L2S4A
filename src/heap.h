#ifndef __HEAP_H
#define __HEAP_H

#include <stdbool.h>

typedef struct key_value {
    int key;
    int value;
} *Key_Value;

typedef struct heap {
    unsigned max_size;
    Key_Value *heap;
    unsigned next;
} *Heap;

Heap new_heap(unsigned max_size);
void free_heap(Heap h);
unsigned left_index(unsigned index);
Key_Value left(Heap h, unsigned index);
unsigned right_index(unsigned index);
Key_Value right(Heap h, unsigned index);
unsigned parent_index(unsigned index);
Key_Value parent(Heap h, unsigned index);
void insert_to_heap(Heap h, int key, int value);
Key_Value remove_from_heap(Heap h);
bool is_heap_empty(Heap h);

#endif