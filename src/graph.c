#include <stdlib.h>
#include <stdbool.h>
#include "list.h"
#include "graph.h"

void free_matrix(Matrix m) {
    if(m == NULL) {
        return;
    }

    for(unsigned i = 0; i < m->nb_vertices; i++) {
        free(m->matrix[i]);
    }
    free(m->matrix);
    free(m);
}

Matrix new_matrix(unsigned nb_vertices) {
    Matrix m = malloc(sizeof(struct matrix));
    m->nb_vertices = nb_vertices;
    m->matrix = malloc(nb_vertices * sizeof(void*));

    for(unsigned i = 0; i < nb_vertices; i++) {
        m->matrix[i] =  malloc(nb_vertices * sizeof(bool));
        for(unsigned j = 0; j < nb_vertices; j++) {
            m->matrix[i][j] = false;
        }
    }
    
    return m;
}

void matrix_add_edge(Matrix m, unsigned src, unsigned dest) {
    m->matrix[src][dest] = false;   
}

bool is_in_matrix(Matrix m, unsigned src, unsigned dest) {
    return m->matrix[src][dest];
}

List matrix_vertex_successors(Matrix m, unsigned src) {
    List list = NULL;
    
    for(unsigned i = 0; i < m->nb_vertices; i++) {
        if(m->matrix[src][i]) {
            list = add_to_list(list, i, NULL);
        }
    }
    
    return list;
}

Vertex new_vertex(unsigned id) {
    Vertex v = malloc(sizeof(struct vertex));
    v->id = id;
    v->edges = NULL;

    return v;
}

AdjacencyList new_adjacency_list(unsigned nb_vertices) {
    AdjacencyList list = malloc(sizeof(struct adjacency));
    list->nb_vertices = nb_vertices;
    list->vertices = malloc(nb_vertices * sizeof(Vertex));
    for(unsigned i = 0; i < nb_vertices; i++) {
        list->vertices[i] = new_vertex(i);
    }

    return list;
}

void adjacency_add_edge(AdjacencyList graph, unsigned src, Edge e) {
    // add at the start of the list : O(1) insert
    List l = new_list(e->id, e);
    l->next = graph->vertices[src]->edges;
    graph->vertices[src]->edges = l;
}

bool is_in_adjacency_list(AdjacencyList graph, unsigned src, unsigned dest) {
    List current = graph->vertices[src]->edges;
    while(current != NULL) {
        if(current->key == dest) {
            return true;
        }
        current = current->next;
    }

    return false;
}

List adjacency_list_successors(AdjacencyList graph, unsigned src) {
    return graph->vertices[src]->edges;
}

void remove_edge_from_vertex(AdjacencyList g, unsigned src, Edge e) { 
    g->vertices[src]->edges = remove_from_list(g->vertices[src]->edges, e->id);
}

void free_vertex(Vertex v) {
    if(v == NULL) {
        return;
    }

    List tmp = v->edges;
    while(tmp != NULL) {
        free_edge(tmp->value);
        tmp = tmp->next;
    }

    free_list(v->edges);
    free(v);
}

void free_adjacency(AdjacencyList al) {
    if(al == NULL) {
        return;
    }
    
    for(unsigned i = 0; i < al->nb_vertices; i++) {
        free_vertex(al->vertices[i]);
    }
    free(al->vertices);
    free(al);
}

Edge new_edge(unsigned id, int w) {
    Edge e = malloc(sizeof(struct edge));
    e->id = id;
    e->weight = w;

    return e;
}

void free_edge(Edge e) {
    free(e);
}

Edge find_edge(AdjacencyList g, unsigned src, unsigned dest) {
    List current = g->vertices[src]->edges;  
    while (current != NULL) {
        if (current->key == dest) {
            return current->value;
        }
        current = current->next;
    }
    return NULL;
}