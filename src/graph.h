#ifndef __GRAPH_H
#define __GRAPH_H

#include <stdlib.h>
#include <stdbool.h>
#include "list.h"

typedef struct matrix {
    unsigned nb_vertices;
	bool **matrix;
} *Matrix;

typedef struct vertex {
    unsigned id;
    struct list* edges;
} *Vertex;

typedef struct edge {
    unsigned id;
    int weight;
} *Edge;

typedef struct adjacency {
    Vertex* vertices;
    unsigned nb_vertices;
} *AdjacencyList;

void free_matrix(Matrix m);
Matrix new_matrix(unsigned nb_vertices);
void matrix_add_edge(Matrix m, unsigned src, unsigned dest);
bool is_in_matrix(Matrix m, unsigned src, unsigned dest);
List matrix_vertex_successors(Matrix m, unsigned src);

AdjacencyList new_adjacency_list(unsigned nb_vertices);
void adjacency_add_edge(AdjacencyList graph, unsigned src, Edge e);
bool is_in_adjacency_list(AdjacencyList graph, unsigned src, unsigned dest);
List adjacency_list_successors(AdjacencyList graph, unsigned src);
void free_adjacency(AdjacencyList al);
Edge find_edge(AdjacencyList g, unsigned src, unsigned dest);

void free_vertex(Vertex v);
Vertex new_vertex(unsigned id);

Edge new_edge(unsigned id, int w);
void free_edge(Edge e);

#endif