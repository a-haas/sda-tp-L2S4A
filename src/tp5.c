#include <stdio.h>
#include <stdlib.h>
#include <limits.h>
#include <assert.h>
#include <time.h>
#include <stdbool.h>

#include "list.h"
#include "graph.h"

unsigned find_priority_element(List to_handle, int *distance) {
    int min = distance[to_handle->key];
    List l = to_handle->next;
    unsigned priority_element = to_handle->key;
    while (l != NULL) {
        if (distance[l->key] < min) {
            priority_element = l->key;
            min = distance[l->key];
        }
        l = l->next;
    }
    return priority_element;
}

Vertex *dijkstra(AdjacencyList g, unsigned s) {
    int distance[g->nb_vertices];
    Vertex *predecessor = malloc(g->nb_vertices * sizeof(struct vertex));
    bool handled[g->nb_vertices];
    for(unsigned i = 0; i < g->nb_vertices; i++) {
        handled[i] = false;
    }
    List to_handle = new_list(s, NULL);
    distance[s] = 0;
    predecessor[s] = NULL;

    for(unsigned i = 0; i < g->nb_vertices; i++) {
        if(i != s) {
            distance[i] = INT_MAX;
            predecessor[i] = NULL;
        }
    }

    while (to_handle != NULL) {
        unsigned priority_element = find_priority_element(to_handle, distance);
        handled[priority_element] = true;
        to_handle = remove_from_list(to_handle, priority_element);

        List l = adjacency_list_successors(g, priority_element);
        while(l != NULL) {
            if (!handled[l->key]) {
                Edge e = (Edge) l->value; 
                int w = e->weight;
                int dist_priority = distance[priority_element];
                int dist_current = distance[e->id];
                if (dist_current > dist_priority + w) {
                    if (dist_current == INT_MAX) {
                        to_handle = add_to_list(to_handle, e->id, NULL);
                    }
                    distance[e->id] = dist_priority + w;
                    predecessor[e->id] = g->vertices[priority_element];
                }
            }
            l =l->next;
        }
    }

    // print results : distance from s to each node of the graph
    // for (int i = 0; i < g->nb_vertices; ++i) {
    //     printf("Distance de %d à %d = %d\n", s, i, distance[i]);
    // }

    free_list(to_handle);
    return predecessor;
}

void measure_dijkstra(char *filename, unsigned n, AdjacencyList g, unsigned s) {
    FILE *file = fopen(filename, "a");
    double total_time = 0.0;
    
    for (int i = 0; i < 10; i++) {
		printf("i = %d\n", i);
        
        clock_t start_t, end_t;
        start_t = clock();
        Vertex *predecessor = dijkstra(g, s);
        end_t = clock();
        free(predecessor);
        total_time += ((double)(end_t - start_t)) / (double)CLOCKS_PER_SEC;;
    }

    if (file != NULL) {
        fprintf(file, "%d %f\n", n, total_time / 10.0);
    }
    fclose(file);
}

int main() {
    unsigned n = 5;
    for (int k = 0; k < 14; k++) {
        n *= 2;
        
		printf("n = %d\n", n);
        
        AdjacencyList graph = new_adjacency_list(n);

        // Initializes random number generator
        time_t t;
        srand((unsigned)time(&t));
        
        // add 10 edges for every vertex 
        for (unsigned i = 0; i < n; i++){
            for(unsigned j = 0; j < 10; j++) {
                int w = rand() % 1000;
                int dest;
                do {
                    dest = rand() % n;
                } while(is_in_adjacency_list(graph, i, dest));

                adjacency_add_edge(graph, i, new_edge(dest, w));
            }
        }
        measure_dijkstra("Dijkstra_List.time", n, graph, rand() % n);

        free_adjacency(graph);
    }

    return 0;
}