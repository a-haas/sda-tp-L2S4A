#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <limits.h>
#include <stdbool.h>
#include <assert.h>

#include "heap.h"

void heap_sort(int *array, unsigned n) {
    Heap h = new_heap(n);

    for(unsigned i = 0; i < n; i++) {
        insert_to_heap(h, array[i], array[i]);
    }

    for(unsigned i = 0; i < n; i++) {
        array[i] = remove_from_heap(h)->value;
    }

 
    free_heap(h);
}

bool array_is_sorted(int *T, unsigned n) {
    for(unsigned i = 0; i < (n-1); i++) {
        if(T[i] > T[i + 1]) {
            return false;
        }
    }

    return true;
}

int main() {

    FILE *heap_file = NULL;
    heap_file = fopen("HeapFile.time", "a");

    unsigned n = 5;
    for (int k = 0; k < 14; k++)
    {
        n *= 2;
        double current_total_time = 0.0;
        printf("n = %d\n", n);
        for (int i = 0; i < 10; i++)
        {
            printf("i = %d\n", i);
            int array[n];

            time_t t;
            
            srand((unsigned)time(&t)); // Initializes random number generator
            for (unsigned i = 0; i < n; i++){
                int val = rand() % INT_MAX;
                array[i] = val;
            }

            clock_t start_t, end_t;
            double total_t;
            
            assert(!array_is_sorted(array, n));
            start_t = clock();
            heap_sort(array, n);
            end_t = clock();
            assert(array_is_sorted(array, n));
            total_t = ((double)(end_t - start_t)) / (double)CLOCKS_PER_SEC;
            current_total_time += total_t;
        }
        current_total_time /= 10.0;
        if (heap_file != NULL)
            fprintf(heap_file, "%d %f\n", n, current_total_time);
    }

    fclose(heap_file);
    return 0;
}
