#include <stdlib.h>
#include <stdbool.h>
#include <math.h>
#include <stdio.h>

#include "heap.h"

Key_Value new_key_value(int key, int value) {
    Key_Value kv = malloc(sizeof(struct key_value));
    kv->key = key;
    kv->value = value;

    return kv;
}

void free_key_value(Key_Value kv) {
    if(kv != NULL) {
        free(kv);
    }
}

Heap new_heap(unsigned max_size) {
    Heap h = malloc(sizeof(struct heap));
    h->max_size = pow(2, ceil(log(max_size)/log(2)));
    h->next = 0;
    h->heap = malloc(h->max_size * sizeof(Key_Value));

    for(unsigned i = 0; i < h->max_size; i++) {
        h->heap[i] = NULL;
    }

    return h;
}

void free_heap(Heap h) {
    // for(unsigned i = 0; i < h->max_size; i++) {
    //     if(h->heap[i] != NULL) {
    //         free_key_value(h->heap[i]);
    //     }
    // }

    free(h->heap);
    free(h);
}

unsigned left_index(unsigned index) {
    unsigned left_index = index * 2 + 1;

    return left_index;
}

Key_Value left(Heap h, unsigned index) {
    return h->heap[left_index(index)];
}

unsigned right_index(unsigned index) {
    unsigned right_index = index * 2 + 2;

    return right_index;
}

Key_Value right(Heap h, unsigned index) {
    return h->heap[right_index(index)];
}

unsigned parent_index(unsigned index) {
    if(index == 0) {
        return 0;
    }

    unsigned parent_index = (index - 1) / 2;

    return parent_index;
}

Key_Value parent(Heap h, unsigned index) {
    return h->heap[parent_index(index)];
}

void insert_to_heap(Heap h, int key, int value) {
    h->heap[h->next] = new_key_value(key, value);
    
    unsigned current = h->next;
    unsigned parent = parent_index(current);

    while(h->heap[parent]->key > h->heap[current]->key) {
        // swap current node with parent to maintain partial ordering
        Key_Value tmp = h->heap[current];
        h->heap[current] = h->heap[parent];
        h->heap[parent] = tmp;

        current = parent;
        parent = parent_index(current);
    }

    h->next += 1;
}

Key_Value remove_from_heap(Heap h) {
    unsigned index = 0;
    Key_Value res = h->heap[index];
    
    Key_Value last = h->heap[h->next - 1];
    h->heap[h->next - 1] = NULL;
    h->next -= 1;
    h->heap[index] = last; 

    unsigned current = index;
    unsigned left = left_index(current);
    unsigned right = right_index(current);
    
    while (
        (left < h->next && (h->heap[current]->key > h->heap[left]->key))
        ||
        (right < h->next && (h->heap[current]->key > h->heap[right]->key))
    ) {
        // by defaut left
        unsigned to_swap = left;
        // if right is still in range choose left or right
        if(right < h->next) {
            to_swap = h->heap[left]->key < h->heap[right]->key 
                ? left : right;
        }
        // swap current node with child
        Key_Value tmp = h->heap[current];
        h->heap[current] = h->heap[to_swap];
        h->heap[to_swap] = tmp;

        current = to_swap;

        left = left_index(current);
        right = right_index(current);
    }

    return res;
}

bool is_heap_empty(Heap h) {
    return h->next <= 0;
}